# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


"""
This package provides access to the different calculations.

.. note::
 * Use the function :func:`base.create_calculation` to create concrete calculations.

"""


# Restrict the public interface
from sample_calculator.calculations.base import CalculationError, create_calculation
from sample_calculator.calculations.calculations.sum_calculation import SUM_CALCULATION
from sample_calculator.calculations.calculations.median_high_calculation import MEDIAN_HIGH_CALCULATION
from sample_calculator.calculations.calculations.average_calculation import AVERAGE_CALCULATION
from sample_calculator.calculations.calculations.median_low_calculation import MEDIAN_LOW_CALCULATION
from sample_calculator.calculations.calculations.median_calculation import MEDIAN_CALCULATION
from sample_calculator.calculations.calculations.square_sum_calculation import SQUARE_SUM_CALCULATION

